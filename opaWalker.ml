(*
    Copyright © 2011 MLstate

    This file is part of Opa.

    Opa is free software: you can redistribute it and/or modify it under the
    terms of the GNU Affero General Public License, version 3, as published by
    the Free Software Foundation.

    Opa is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
    more details.

    You should have received a copy of the GNU Affero General Public License
    along with Opa. If not, see <http://www.gnu.org/licenses/>.
*)

let _output_channel = stderr

let walk_anim = [|
  [|
    "  @ ";
    "</| ";
    ")\\| "
  |] ; [|
    "  @ ";
    "</\\ ";
    ")\\ \\";
  |] ; [|
    "  @ ";
    "</ \\";
    ")\\ |";
  |] ; [|
    "  @ ";
    "// \\";
    "/ )|";
  |] ; [|
    " _ @";
    "  /|";
    "/ )|";
  |] ; [|
    " _ @";
    "  /|";
    " )\\|"
  |] |]

let turn_anim = [|
  [|
    " @ ";
    " | ";
    " )\\";
  |] ; [|
    " @ ";
    " | ";
    "/( "
  |] |]

let t =
  ConsoleAnim.init ~och:stdout ~auto_walking:true walk_anim turn_anim

let () =
  while true do
    ConsoleAnim.update t;
    Unix.sleepf 0.2
  done
